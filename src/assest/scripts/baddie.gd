extends KinematicBody

export var anim_col = 0
export var speed = 7
export var gravity = 20
const ACCEL_DEFAULT = 7
const ACCEL_AIR = 1
onready var accel = ACCEL_DEFAULT

var direction = Vector3()
var velocity = Vector3()
var gravity_vec = Vector3()
var movement = Vector3()
var snap
var jump = 10

onready var sprite = $Sprite3D
onready var ani_player = $AnimationPlayer

var camera = null

func _ready():
	add_to_group("baddies")
	ani_player.play("anim_col")

func _physics_process(delta):
	follow_camera()
	jump(delta)
	move(delta)

func set_camera(c):
	camera = c
	
func follow_camera():
	if camera == null:
		return
	
	var p_fwd = -camera.global_transform.basis.z
	var fwd = global_transform.basis.z
	var left = global_transform.basis.x
	
	var l_dot = left.dot(p_fwd)
	var f_dot = fwd.dot(p_fwd)
	var row = 0
	sprite.flip_h = false
	if f_dot < -8.5:
		row = 0 # front sprite
	elif f_dot > 8.5:
		row = 4 # back sprite
	else:
		sprite.flip_h = l_dot > 0
		if abs(f_dot) < 0.3:
			row = 2 # left sprite
		elif f_dot < 0:
			row = 1 # forward left sprite
		else:
			row = 3 # back left sprite
	sprite.frame = anim_col + row * 4

func jump(delta):
	#jumping and gravity
	if is_on_floor():
		snap = -get_floor_normal()
		accel = ACCEL_DEFAULT
		gravity_vec = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		accel = ACCEL_AIR
		gravity_vec += Vector3.DOWN * gravity * delta
		
	#if Input.is_action_just_pressed("jump") and is_on_floor():
	#	snap = Vector3.ZERO
	#	gravity_vec = Vector3.UP * jump

func move(delta):
	#make it move
	velocity = velocity.linear_interpolate(direction * speed, accel * delta)
	movement = velocity + gravity_vec
	
	move_and_slide_with_snap(movement, snap, Vector3.UP)
